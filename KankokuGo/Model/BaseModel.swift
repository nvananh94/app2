//
//  AccountModel.swift
//  SupportCustomer
//
//  Created by Nguyen Van Anh on 3/7/19.
//  Copyright © 2019 Nguyen Van Anh. All rights reserved.
//

import UIKit
import EVReflection


class BaseResponse: EVObject {
    var jsonrpc: String?
    var code: NSNumber?
    var error: String?
    var message: String?
    var id: NSNumber?
}

class GetDataResponse: BaseResponse {
    var result: String?
    var data: GetData?
}

class GetData: BaseResponse {
    var terms: Terms?
    var posts: Posts?
}

class Terms: BaseResponse {
    var current_page: NSNumber?
    var data: [DataTerm]?
    var first_page_url: String?
    var from: String?
    var last_page: NSNumber?
    var last_page_url: String?
    var next_page_url: String?
    var path: String?
    var per_page: NSNumber?
    var prev_page_url: String?
    var to: NSNumber?
    var total: NSNumber?
}

class DataTerm: BaseResponse {
    var term_id: NSNumber?
    var name: String?
    var slug: String?
    var term_group: NSNumber?
}

class Posts: BaseResponse {
    var current_page: NSNumber?
    var data: [DataPosts]?
    var first_page_url: String?
    var from: String?
    var last_page: NSNumber?
    var last_page_url: String?
    var next_page_url: String?
    var path: String?
    var per_page: NSNumber?
    var prev_page_url: String?
    var to: NSNumber?
    var total: NSNumber?
}

class DataPosts: BaseResponse {
    var ID: NSNumber?
    var post_title: String?
    var post_content: String?
    var post_date_gmt: String?
    var post_date: String?
    var post_author: NSNumber?
}
