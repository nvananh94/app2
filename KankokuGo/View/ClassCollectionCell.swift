//
//  ClassCollectionCell.swift
//  KankokuGo
//
//  Created by tld on 4/3/19.
//  Copyright © 2019 tld. All rights reserved.
//

import UIKit

class ClassCollectionCell: UICollectionViewCell {
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var widthItem: NSLayoutConstraint!
}
