//
//  TitleViewCell.swift
//  KankokuGo
//
//  Created by Nguyen Van Anh on 04/09/2019.
//  Copyright © 2019 tld. All rights reserved.
//

import UIKit

class TitleViewCell: UITableViewCell {
    @IBOutlet weak var txtTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
