//
//  UIViewController+.swift
//  ShuwaTaishi
//
//  Created by tld on 10/3/18.
//  Copyright © 2018 tld. All rights reserved.
//

import UIKit
import SVProgressHUD

enum StoryboardName: String {
    case main = "Main"
    case search = "Search"
    case home = "Home"
    case purchase = "Purchase"
}

extension UIViewController {
    var isModal: Bool {
        return presentingViewController != nil ||
            navigationController?.presentingViewController?.presentedViewController === navigationController ||
            tabBarController?.presentingViewController is UITabBarController
    }
    
    var topbarHeight: CGFloat {
        return UIApplication.shared.statusBarFrame.size.height +
            (self.navigationController?.navigationBar.frame.height ?? 0.0)
    }
}

extension UIViewController {
    
    func instantiateViewController<T>(ofType type: T.Type) -> T {
        return storyboard(name: .main).instantiateViewController(ofType: type)
    }
}

extension UIViewController {
    
    func instantiateViewController<T>(fromStoryboard name: StoryboardName, ofType type: T.Type) -> T {
        return storyboard(name: name).instantiateViewController(ofType: type)
    }
    
    func storyboard(name: StoryboardName) -> UIStoryboard {
        return UIStoryboard(name: name.rawValue, bundle: nil)
    }
    
    func showLoading(message: String = "", isShowMask: Bool = false) {
        if isShowMask {
            SVProgressHUD.setDefaultMaskType(.clear)
        }
        if !message.isEmpty {
            SVProgressHUD.show(withStatus: message)
        } else {
            SVProgressHUD.show()
        }
    }
    
    @objc func dismissLoading() {
        SVProgressHUD.dismiss()
    }
    
}
