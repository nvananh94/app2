//
//  UIDevice+.swift
//  ShuwaTaishi
//
//  Created by tld on 10/23/18.
//  Copyright © 2018 tld. All rights reserved.
//

import UIKit

extension UIDevice {
    static func isPad() -> Bool {
        return self.current.userInterfaceIdiom == .pad
    }
}
