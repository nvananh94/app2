//
//  UIStoryboard+.swift
//  CQMS
//
//  Created by tld on 7/26/18.
//  Copyright © 2018 tld. All rights reserved.
//

import UIKit

extension UIStoryboard {
    // swiftlint:disable force_cast
    func instantiateViewController<T>(ofType type: T.Type) -> T {
        return instantiateViewController(withIdentifier: String(describing: type)) as! T
    }
}
