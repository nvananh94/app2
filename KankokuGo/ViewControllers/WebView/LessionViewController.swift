//
//  LessionViewController.swift
//  AKSwiftSlideMenu
//
//  Created by MAC-186 on 4/8/16.
//  Copyright © 2016 Kode. All rights reserved.
//
//

import UIKit
import Parchment

class LessionViewController: BaseViewController {
    @IBOutlet weak var viewContent: UIView!
    var index_pop: Int?
    var is_view_translate: Bool = true
    let translate = TranslateViewController(nibName: "TranslateViewController", bundle: nil)
    let grammar = GrammarViewController(nibName: "GrammarViewController", bundle: nil)
    var lop_hoc: String = ""
    var hour: String = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        self.showAds()
        var controllers : [UIViewController] = []
        translate.title = "DỊCH"
        translate.lop = self.lop_hoc
        translate.hour = self.hour
        controllers.append(translate)
        grammar.title = "NGỮ PHÁP"
        grammar.lop = self.lop_hoc
        grammar.hour = self.hour
        controllers.append(grammar)
        
        let pagingViewController = FixedPagingViewController(viewControllers: controllers)
        self.addChildViewController(pagingViewController)
        self.viewContent.addSubview(pagingViewController.view)
        self.viewContent.constrainToEdges(pagingViewController.view)
        pagingViewController.didMove(toParentViewController: self)
        pagingViewController.selectedTextColor = UIColor.white
        pagingViewController.indicatorColor = UIColor.red
        pagingViewController.backgroundColor = UIColor().navigationColor()
        pagingViewController.selectedBackgroundColor = UIColor().navigationColor()
        pagingViewController.indicatorOptions = .visible(
            height: 2,
            zIndex: Int.max,
            spacing: .zero,
            insets: .zero
        )
    }
}






//import UIKit
//import SwiftSoup
//
//class LessionViewController: BaseViewController {
//
//    @IBOutlet weak var tableView: UITableView!
//    var arrayHtml = [Dictionary<String,String>]()
//    var link = ""
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        addSlideMenuButton()
//        self.getData()
//        // Do any additional setup after loading the view.
//    }
//
//    override func didReceiveMemoryWarning() {
//        super.didReceiveMemoryWarning()
//        // Dispose of any resources that can be recreated.
//    }
//
//    func parseHtml() {
//        guard let url = URL(string: link) else { return }
//        guard let html = try? String(contentsOf: url, encoding: .utf8) else { return }
//        do {
//            let doc: Document = try SwiftSoup.parseBodyFragment(html)
//            // my body
//            let body = doc.body()
//            // elements to remove, in this case images
//            guard let mucluc: Elements? = try body?.select("div#mucluc") else { return }
//            guard let link: Elements = try mucluc?.select("a") else { return }
//            for element: Element in link.array() {
//                arrayHtml.append(["link":try element.attr("href"), "title":try element.text()])
//            }
//        } catch Exception.Error( _, let message) {
//            print("Message: \(message)")
//        } catch {
//            print("error")
//        }
//    }
//}
//
//// MARK: - Table view delegate
//
//extension LessionViewController: UITableViewDelegate, UITableViewDataSource {
//
//    func numberOfSections(in tableView: UITableView) -> Int {
//        return 1
//    }
//
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return arrayHtml.count
//    }
//
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 60
//    }
//
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        if let cell : UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "cellHtml") {
//            cell.selectionStyle = UITableViewCellSelectionStyle.none
//            cell.layoutMargins = UIEdgeInsets.zero
//            cell.preservesSuperviewLayoutMargins = false
//            cell.backgroundColor = UIColor.clear
//
//            let lblTitle : UILabel = cell.contentView.viewWithTag(102) as! UILabel
//            lblTitle.text = arrayHtml[indexPath.row]["title"]
//
//            return cell
//        }
//        return UITableViewCell()
//    }
//
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        tableView.deselectRow(at: indexPath, animated: true)
//        let webVC = self.instantiateViewController(fromStoryboard: .main, ofType: WebViewController.self)
//        webVC.link = arrayHtml[indexPath.row]["link"] ?? ""
//        self.navigationController?.pushViewController(webVC, animated: true)
//    }
//}
//
//
