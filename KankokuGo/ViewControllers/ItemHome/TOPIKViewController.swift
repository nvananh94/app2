//
//  TOPIKViewController.swift
//  KankokuGo
//
//  Created by Nguyen Van Anh on 04/11/2019.
//  Copyright © 2019 tld. All rights reserved.
//

import UIKit

class TOPIKViewController: BaseViewController{
    @IBOutlet weak var collectionView: UICollectionView!
    var arrayDataClass = [Dictionary<String,String>]()
    let column: CGFloat = 2
    let minItemSpacingIphone: CGFloat = 50.0
    let minItemSpacingIpad: CGFloat = 50.0
    let leftItemSpacingIphone: CGFloat = 20.0
    let leftItemSpacingIpad: CGFloat = 30.0
    var totalWidth: CGFloat = 0
    var titleNav: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = title
        addSlideMenuButton()
        setupDataClass()
        self.showAds()
    }
    
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    func setupDataClass() {
        arrayDataClass.append(["name":"TOPIK I", "class": "lop1", "hour": "0h"])
        arrayDataClass.append(["name":"TOPIK II", "class": "lop2", "hour": "0h"])
        collectionView.reloadData()
    }
    
    private func getItemWidth(boundWidth: CGFloat) -> CGFloat {
        if UIDevice.isPad() {
            totalWidth = boundWidth - ((column - 1) * minItemSpacingIpad) - (leftItemSpacingIpad * 2)
        } else {
            totalWidth = boundWidth - ((column - 1) * minItemSpacingIphone) - (leftItemSpacingIphone * 2)
        }
        return totalWidth / column
    }
    
    private func getMarginTop(itemWidth: CGFloat, boundWidth: CGFloat) -> CGFloat {
        var marginTopBottom: CGFloat = 20.0
        let count_item = CGFloat(self.arrayDataClass.count)
        if UIDevice.isPad() {
            let height_all_item = (itemWidth * count_item + ((count_item - 1) * minItemSpacingIpad))
            if height_all_item >= boundWidth {
                let height = height_all_item - boundWidth
                if height > 40 {
                    marginTopBottom = height / 2
                }
            }
            return marginTopBottom
        } else {
            let height_all_item = (itemWidth * count_item + ((count_item - 1) * minItemSpacingIphone))
            if height_all_item <= boundWidth {
                let height = boundWidth - height_all_item
                if height > 40 {
                    marginTopBottom = height / 2
                }
            }
            return marginTopBottom
        }
    }
}

extension TOPIKViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        print("count", arrayDataClass.count)
        return arrayDataClass.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(ofType: ClassCollectionCell.self, for: indexPath) {
            let itemWidth = getItemWidth(boundWidth: collectionView.bounds.size.width)
            cell.nameLabel?.text = arrayDataClass[indexPath.row]["name"] ?? ""
            cell.widthItem.constant = itemWidth
            return cell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let itemWidth = getItemWidth(boundWidth: collectionView.bounds.size.width)
        return CGSize(width: collectionView.bounds.size.width, height: itemWidth)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let name = arrayDataClass[indexPath.row]["name"] ?? ""
        let topik = self.instantiateViewController(fromStoryboard: .main, ofType: TOPIKOtherViewController.self)
        topik.title = name
        self.navigationController?.pushViewController(topik, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        let itemWidth = getItemWidth(boundWidth: collectionView.bounds.size.width)
        let marginTopBottom = self.getMarginTop(itemWidth: itemWidth, boundWidth: collectionView.bounds.size.height)
        if UIDevice.isPad() {
            return UIEdgeInsets(top: marginTopBottom, left: leftItemSpacingIpad, bottom: marginTopBottom, right: leftItemSpacingIpad)
        } else {
            return UIEdgeInsets(top: marginTopBottom, left: leftItemSpacingIphone, bottom: marginTopBottom, right: leftItemSpacingIphone)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        if UIDevice.isPad() {
            return minItemSpacingIpad
        } else {
            return minItemSpacingIphone
        }
    }
    
}

