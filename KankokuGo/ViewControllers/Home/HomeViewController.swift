//
//  HomeViewController.swift
//  KankokuGo
//
//  Created by tld on 3/27/19.
//  Copyright © 2019 tld. All rights reserved.
//

import UIKit
enum Key: String {
    case topik, kiip, grammar, translate, multiple_choice, study
}

class HomeViewController: BaseViewController{
    @IBOutlet weak var collectionView: UICollectionView!
    var arrayDataClass = [Dictionary<String,String>]()
    let column: CGFloat = 2
    let minItemSpacingIphone: CGFloat = 20.0
    let minItemSpacingIpad: CGFloat = 30.0
    let leftItemSpacingIphone: CGFloat = 20.0
    let leftItemSpacingIpad: CGFloat = 30.0
    var totalWidth: CGFloat = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addSlideMenuButton()
        setupDataClass()
        self.showAds()
    }
    

    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    func setupDataClass() {
        arrayDataClass.append(["name":"TOPIK", "key": Key.topik.rawValue, "hour": "0h"])
        arrayDataClass.append(["name":"KIIP", "key": Key.kiip.rawValue, "hour": "0h"])
        arrayDataClass.append(["name":"NGỮ PHÁP", "key": Key.grammar.rawValue, "hour": "0h"])
        arrayDataClass.append(["name":"TỪ VỰNG", "key": Key.translate.rawValue, "hour": "0h"])
        arrayDataClass.append(["name":"TRẮC NGHIỆM", "key": Key.multiple_choice.rawValue, "hour": "20h"])
        arrayDataClass.append(["name":"DU HỌC", "key": Key.study.rawValue, "hour": "50h"])
        collectionView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func getItemWidth(boundWidth: CGFloat) -> CGFloat {
        if UIDevice.isPad() {
            totalWidth = boundWidth - ((column - 1) * minItemSpacingIpad) - (leftItemSpacingIpad * 2)
        } else {
            totalWidth = boundWidth - ((column - 1) * minItemSpacingIphone) - (leftItemSpacingIphone * 2)
        }
        return totalWidth / column
    }
    
    private func getMarginTop(itemWidth: CGFloat, boundWidth: CGFloat) -> CGFloat {
        var marginTopBottom: CGFloat = 20.0
        let count_item = CGFloat(self.arrayDataClass.count / Int(self.column))
        if UIDevice.isPad() {
            let height_all_item = (itemWidth * count_item + ((count_item - 1) * minItemSpacingIpad))
            if height_all_item >= boundWidth {
                let height = height_all_item - boundWidth
                if height > 40 {
                    marginTopBottom = height / 2
                }
            }
            return marginTopBottom
        } else {
            let height_all_item = (itemWidth * count_item + ((count_item - 1) * minItemSpacingIphone))
            if height_all_item <= boundWidth {
                let height = boundWidth - height_all_item
                if height > 40 {
                    marginTopBottom = height / 2
                }
            }
            return marginTopBottom
        }
    }
}

extension HomeViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        print("count", arrayDataClass.count)
        return arrayDataClass.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(ofType: ClassCollectionCell.self, for: indexPath) {
            cell.nameLabel?.text = arrayDataClass[indexPath.row]["name"] ?? ""
            return cell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let itemWidth = getItemWidth(boundWidth: collectionView.bounds.size.width)
        return CGSize(width: itemWidth, height: itemWidth)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let item = arrayDataClass[indexPath.row]
        switch item["key"] {
        case Key.topik.rawValue:
            let name = arrayDataClass[indexPath.row]["name"] ?? ""
            let topik = self.instantiateViewController(fromStoryboard: .main, ofType: TOPIKViewController.self)
            topik.title = name
            self.navigationController?.pushViewController(topik, animated: true)
        case Key.kiip.rawValue:
            let url_fb = "https://itunes.apple.com/app/id1458634673"
            if url_fb != "" {
                self.openURL(url_fb, "")
            }
        case Key.grammar.rawValue:
            let url_fb = "https://itunes.apple.com/app/id1458634673"
            if url_fb != "" {
                self.openURL(url_fb, "")
            }
        case Key.translate.rawValue:
            let url_fb = "https://itunes.apple.com/app/id1458634673"
            if url_fb != "" {
                self.openURL(url_fb, "")
            }
        case Key.multiple_choice.rawValue:
            let url_fb = "https://itunes.apple.com/app/id1458634673"
            if url_fb != "" {
                self.openURL(url_fb, "")
            }
        case Key.study.rawValue:
            let url_fb = "https://itunes.apple.com/app/id1458634673"
            if url_fb != "" {
                self.openURL(url_fb, "")
            }
        default:
            break
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        let itemWidth = getItemWidth(boundWidth: collectionView.bounds.size.width)
        let marginTopBottom = self.getMarginTop(itemWidth: itemWidth, boundWidth: collectionView.bounds.size.height)
        if UIDevice.isPad() {
            return UIEdgeInsets(top: marginTopBottom, left: leftItemSpacingIpad, bottom: marginTopBottom, right: leftItemSpacingIpad)
        } else {
            return UIEdgeInsets(top: marginTopBottom, left: leftItemSpacingIphone, bottom: marginTopBottom, right: leftItemSpacingIphone)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        if UIDevice.isPad() {
            return minItemSpacingIpad
        } else {
            return minItemSpacingIphone
        }
    }
    
}

