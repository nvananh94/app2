//
//  MNConstan.swift
//  SupportCustomer
//
//  Created by Nguyen Van Anh on 3/7/19.
//  Copyright © 2019 Nguyen Van Anh. All rights reserved.
//

import UIKit

class Constant: NSObject {
    static let api_solop = "http://apphanquoclythu.com/api/lumen-core/public/v1/terms/"
    static let api_danhmuc = "http://apphanquoclythu.com/api/lumen-core/public/v1/posts/term/"
    static let format_dmyhms = "format_dmyhms"
    static let format_dmy = "format_dmy"
    static let ymdhms: String = "yyyy-MM-dd HH:mm:ss"
    static let dmyhms: String = "dd-MM-yyyy HH:mm:ss"
    static let ymd: String = "yyyy-MM-dd"
    static let dmy: String = "dd-MM-yyyy"

    static let width_border: CGFloat = 0.7
    static let corner_radius: CGFloat = 6
    
}
