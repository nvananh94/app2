//
//  BaseViewController.swift
//  AKSwiftSlideMenu
//
//  Created by Ashish on 21/09/15.
//  Copyright (c) 2015 Kode. All rights reserved.
//

import UIKit
import FBSDKShareKit
import JGProgressHUD
import GoogleMobileAds

class BaseViewController: UIViewController, SlideMenuDelegate, GADBannerViewDelegate {
    var hud = JGProgressHUD(style: .dark)
    var banner: GADBannerView!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.showButtonLeft(img: "ic_nav_back")
    }
    
    func showButtonLeft(img: String) {
        self.navigationItem.leftBarButtonItem = nil
        self.navigationItem.rightBarButtonItems = nil
        let leftButton = UIBarButtonItem(image: UIImage(named: img), style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.handleLeftBar))
        self.navigationItem.leftBarButtonItem = leftButton
        self.navigationItem.hidesBackButton = true
    }
    
    @objc func handleLeftBar(){
        self.navigationController?.popViewController(animated: true)
    }
    
    func showAds(){
        banner = GADBannerView(adSize: kGADAdSizeBanner)
        addBannerViewToView(banner)
        banner.adUnitID = "ca-app-pub-3940256099942544/2934735716" // id test
//        banner.adUnitID = "ca-app-pub-2138414238273550~7545108792" // id con that
        banner.rootViewController = self
        banner.load(GADRequest())
        banner.delegate = self
        self.banner.alpha = 0
    }

    func addBannerViewToView(_ bannerView: GADBannerView) {
        bannerView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(bannerView)
        view.addConstraints(
            [NSLayoutConstraint(item: bannerView,
                                attribute: .bottom,
                                relatedBy: .equal,
                                toItem: bottomLayoutGuide,
                                attribute: .top,
                                multiplier: 1,
                                constant: 0),
             NSLayoutConstraint(item: bannerView,
                                attribute: .centerX,
                                relatedBy: .equal,
                                toItem: view,
                                attribute: .centerX,
                                multiplier: 1,
                                constant: 0)
            ])
    }
    
    // banner setup
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        self.banner.alpha = 1
    }
    
    func adView(_ bannerView: GADBannerView, didFailToReceiveAdWithError error: GADRequestError) {
        self.banner.alpha = 0
    }
    
    // banner end setup
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func slideMenuItemSelectedAtIndex(_ index: Int32) {
//        let topViewController : UIViewController = self.navigationController!.topViewController!
//        print("View Controller is : \(topViewController) \n", terminator: "")
        switch(index){
        case 0:
            self.openViewControllerBasedOnIdentifier("HomeViewController")
            break
        case 1:
            faceBookShare()
        case 2:
            let url_fb = "https://www.facebook.com/"
            let appURL = "fb://"
            if url_fb != "" {
                self.openURL(url_fb, appURL)
            }
//            let webVC = self.instantiateViewController(fromStoryboard: .main, ofType: WebViewController.self)
//            webVC.link = "https://www.facebook.com/"
//            self.navigationController?.pushViewController(webVC, animated: true)
            break
        case 3:
            let url_fb = "https://www.youtube.com/"
            if url_fb != "" {
                self.openURL(url_fb, "")
            }
//            let webVC = self.instantiateViewController(fromStoryboard: .main, ofType: WebViewController.self)
//            webVC.link = "https://www.youtube.com/"
//            self.navigationController?.pushViewController(webVC, animated: true)
            break
        default:
            print("default\n", terminator: "")
        }
    }
    
    func openViewControllerBasedOnIdentifier(_ strIdentifier:String){
        if let wd = UIApplication.shared.delegate?.window {
            wd!.rootViewController = UIStoryboard(name: "Main", bundle: nil).instantiateInitialViewController()
        }

//        let destViewController : UIViewController = self.storyboard!.instantiateViewController(withIdentifier: strIdentifier)
//
//        let topViewController : UIViewController = self.navigationController!.topViewController!
        
//        if (topViewController.restorationIdentifier! == destViewController.restorationIdentifier!){
//            print("Same VC")
//        } else {
//            self.navigationController!.pushViewController(destViewController, animated: true)
//        }
    }
    
    func faceBookShare(){
        let content = FBSDKShareLinkContent()
        content.contentURL =  URL(string: "http://itunes.apple.com/app/id1458634673")
        let dialog : FBSDKShareDialog = FBSDKShareDialog()
        dialog.fromViewController = self
        dialog.shareContent = content
        dialog.mode = FBSDKShareDialogMode.feedWeb
        dialog.show()
    }
    
    func addSlideMenuButton(){
        let btnShowMenu = UIButton(type: UIButtonType.custom)
        btnShowMenu.setImage(UIImage (named: "ic_menu_black_24dp"), for: UIControlState())
        btnShowMenu.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btnShowMenu.addTarget(self, action: #selector(BaseViewController.onSlideMenuButtonPressed(_:)), for: UIControlEvents.touchUpInside)
        let customBarItem = UIBarButtonItem(customView: btnShowMenu)
        self.navigationItem.rightBarButtonItem = customBarItem;
    }
    
    @objc func onSlideMenuButtonPressed(_ sender : UIButton){
        if (sender.tag == 10)
        {
            // To Hide Menu If it already there
            self.slideMenuItemSelectedAtIndex(-1);
            
            sender.tag = 0;
            
            let viewMenuBack : UIView = view.subviews.last!
            
            UIView.animate(withDuration: 0.3, animations: { () -> Void in
                var frameMenu : CGRect = viewMenuBack.frame
                frameMenu.origin.x = 1 * UIScreen.main.bounds.size.width
                viewMenuBack.frame = frameMenu
                viewMenuBack.layoutIfNeeded()
                viewMenuBack.backgroundColor = UIColor.clear
                }, completion: { (finished) -> Void in
                    viewMenuBack.removeFromSuperview()
            })
            
            return
        }
        
        navigationController?.setNavigationBarHidden(true, animated: true)
        sender.isEnabled = false
        sender.tag = 10
        
        let menuVC : MenuViewController = self.storyboard!.instantiateViewController(withIdentifier: "MenuViewController") as! MenuViewController
        menuVC.btnMenu = sender
        menuVC.delegate = self
        self.view.addSubview(menuVC.view)
        self.addChildViewController(menuVC)
        menuVC.view.layoutIfNeeded()
        
        
        menuVC.view.frame=CGRect(x: UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height);
        
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            menuVC.view.frame=CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height);
            sender.isEnabled = true
            }, completion:nil)
    }
    
    func animateView(){
        UIView.animate(withDuration: 0.3, animations: {
            self.view.layoutIfNeeded()
        })
    }
}

extension BaseViewController { /* Check nil value
     *************************************************/
    func openURL(_ url: String, _ app: String){
        guard let url = URL(string: url) else {
            return
        }
        if app != "", let app = URL(string: app) {
            if UIApplication.shared.canOpenURL(app) {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(app, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(app)
                }
            }else {
                //redirect to safari because the user doesn't have Instagram
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        }else {
            //redirect to safari because the user doesn't have Instagram
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    func nilString(_ value: String?) -> String{
        if let v = value {
            return v
        }
        return ""
    }
    
    func nilNumber(_ value: NSNumber?) -> NSNumber{
        if let n = value {
            return n
        }
        return 0
    }
    
    func nilInt(_ value: Int?) -> Int{
        if let n = value {
            return n
        }
        return 0
    }
    
    func nilFloat(_ value: Float?) -> Float{
        if let n = value {
            return n
        }
        return 0
    }
}
